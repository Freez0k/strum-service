# frozen_string_literal: true

module Strum
  module Service
    VERSION = "0.2.1"
  end
end
